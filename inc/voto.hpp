#ifndef VOTO_HPP
#define VOTO_HPP
#include <string>
#include "candidato.hpp"


using namespace std;

class Voto{

private:

	string nome_eleitor;
	string deputado_federal;
	string deputado_distrital;
  string senador1;
	string senador2;
  string governador;
  string vice_governador;
  string presidente;
  string vice_presidente;


public:

Voto();
	~Voto();	
Voto(string,string,string,string,string,string,string);


	string get_nome_eleitor();
	void set_nome_eleitor(string nome_eleitor);

	string get_deputado_federal();
	void set_deputado_federal(string deputado_federal);

	string get_deputado_distrital();
	void set_deputado_distrital(string deputado_distrital);

  string get_senador1();
	void set_senador1(string senador2);

  string get_senador2();
  void set_senador2(string senador2);

  string get_governador();
  void set_governador(string governador);

  string get_presidente();
  void set_presidente(string presidente);


};

#endif
