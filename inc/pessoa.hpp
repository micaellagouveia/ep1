#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>

using namespace std;

class Pessoa{

private:
	string nome;
	string idade;
	string genero;


public:
	Pessoa();
	~Pessoa();

	string get_nome();
	void set_nome(string nome);

	string get_idade();
	void set_idade(string idade);

  	string get_genero();
	void set_genero(string genero);

};

#endif
