#ifndef CADASTRO_HPP
#define CADASTRO_HPP
#include <string>
#include "candidato.hpp"

using namespace std;

class Cadastro{

public:
    Cadastro();
    ~Cadastro();
 vector<Candidato> cadastro_candidatosDF();
 vector<Candidato> cadastro_candidatosBR();
};







#endif 