#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include "pessoa.hpp"
#include <string>

using namespace std;

class Candidato : public Pessoa {

private:
  string nr_urna;
  string uf;
  string cargo;
  string nome_urna;
  string nome_partido;
  

public:
  //Candidato();
  int cont;
  

  ~Candidato();
  Candidato(string,string,string,string, string, string, string, string,int);

  

  string get_nr_urna();
  void set_nr_urna(string nr_urna);

  string get_uf();
  void set_uf(string uf);

  string get_cargo();
  void set_cargo(string cargo);

  string get_nome_urna();
  void set_nome_urna(string nome_urna);

  string get_nome_partido();
  void set_nome_partido(string nome_partido);

};




#endif
