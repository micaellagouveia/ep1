#include "candidato.hpp"
#include <iostream>

Candidato::Candidato(string nome, string idade,string genero, string nr_urna, string uf, string cargo, string nome_urna, string nome_partido,int cont){
  set_nome(nome);
  set_idade(idade);
  set_genero(genero);
  set_nr_urna(nr_urna);
  set_uf(uf);
  set_cargo(cargo);
  set_nome_urna(nome_urna);
  set_nome_partido(nome_partido);
  this -> cont = cont;
}

Candidato::~Candidato(){}


string Candidato::get_nr_urna(){
  return nr_urna;
}
void Candidato::set_nr_urna(string nr_urna){
  this-> nr_urna = nr_urna;
}
string Candidato::get_uf(){
  return uf;
}
void Candidato::set_uf(string uf){
  this-> uf = uf;
}
string Candidato::get_cargo(){
  return cargo;
}
void Candidato::set_cargo(string cargo){
  this-> cargo = cargo;
}
string Candidato::get_nome_urna(){
  return nome_urna;
}
void Candidato::set_nome_urna(string nome_urna){
  this-> nome_urna = nome_urna;
}
string Candidato::get_nome_partido(){
  return nome_partido;
}
void Candidato::set_nome_partido(string nome_partido){
  this-> nome_partido = nome_partido;
}
