#include <iostream>
#include <string>
#include <vector>
#include "cadastro.hpp"
#include <fstream>

using namespace std;

Cadastro::Cadastro(){}
Cadastro::~Cadastro(){}
vector <Candidato> Cadastro::cadastro_candidatosDF(){
string nome, idade, genero, nr_urna, uf, cargo, nome_urna, nome_partido;
  int cont = 0;
  vector<Candidato> candidatosDF; //vetor do tipo Candidato chamado candidatosDF

  ifstream arq1("data/consulta_cand_2018_DF2.csv");

  if (arq1.is_open())
  {

    for (int i = 0; i < 1249; i++)
    {
      getline(arq1, nome, ',');
      getline(arq1, idade, ',');
      getline(arq1, genero, ',');
      getline(arq1, nr_urna, ',');
      getline(arq1, uf, ',');
      getline(arq1, cargo, ',');
      getline(arq1, nome_urna, ',');
      getline(arq1, nome_partido, '\n');
      Candidato candidatoDF = Candidato(nome, idade, genero, nr_urna, uf, cargo, nome_urna, nome_partido, cont); //instanciando candidato pelo método encontrado em candidato.hpp
      candidatosDF.push_back(candidatoDF);                                                                       //alocando cada candidato no vetor candidatosDF
    }
  }

  arq1.close();
  return candidatosDF;
}
vector <Candidato> Cadastro::cadastro_candidatosBR(){
    string nome, idade, genero, nr_urna, uf, cargo, nome_urna, nome_partido;
    int cont=0;
    vector<Candidato> candidatosBR; //vetor do tipo Candidato chamado candidatosDF 
  ifstream arq2("data/consulta_cand_2018_BR1.csv");

  if (arq2.is_open())
  {

    for (int c = 0; c < 28; c++)
    {
      getline(arq2, nome, ',');
      getline(arq2, idade, ',');
      getline(arq2, genero, ',');
      getline(arq2, nr_urna, ',');
      getline(arq2, uf, ',');
      getline(arq2, cargo, ',');
      getline(arq2, nome_urna, ',');
      getline(arq2, nome_partido, '\n');
      Candidato candidatoBR = Candidato(nome, idade, genero, nr_urna, uf, cargo, nome_urna, nome_partido, cont);
      candidatosBR.push_back(candidatoBR);
    }
  }
  arq2.close();
  return candidatosBR;
}


