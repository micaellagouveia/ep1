#include "voto.hpp"
#include <iostream>
Voto::Voto(){
  nome_eleitor = "";
  deputado_federal= "";
  deputado_distrital = "";
  senador1 = "";
  senador2 = "";
  governador = "";
  presidente = "";
}

Voto::Voto(string nome_eleitor,string deputado_federal, string deputado_distrital,string senador1, string senador2, string governador, string presidente){
  set_nome_eleitor(nome_eleitor);
  set_deputado_federal(deputado_federal);
  set_deputado_distrital(deputado_distrital);
  set_senador1(senador1);
  set_senador2(senador2);
  set_governador(governador);
  set_presidente(presidente);
}


Voto::~Voto(){}

string Voto::get_nome_eleitor(){
  return nome_eleitor;
}
void Voto::set_nome_eleitor(string nome_eleitor){
  this -> nome_eleitor = nome_eleitor;
}
string Voto::get_deputado_federal(){
  return deputado_federal;
}
void Voto::set_deputado_federal(string deputado_federal){
  this-> deputado_federal = deputado_federal;
}
string Voto::get_deputado_distrital(){
  return deputado_distrital;
}
void Voto::set_deputado_distrital(string deputado_distrital){
  this-> deputado_distrital = deputado_distrital;
}
string Voto::get_senador1(){
  return senador1;
}
void Voto::set_senador1(string senador1){
  this-> senador1 = senador1;
}
string Voto::get_senador2(){
  return senador2;
}
void Voto::set_senador2(string senador2){
  this-> senador2 = senador2;
}
string Voto::get_governador(){
  return governador;
}
void Voto::set_governador(string governador){
  this-> governador = governador;
}
string Voto::get_presidente(){
  return presidente;
}
void Voto::set_presidente(string presidente){
  this->presidente = presidente;
}
