#include <iostream>
#include <fstream>
#include <vector>
#include "voto.hpp"
#include "candidato.hpp"
#include "cadastro.hpp"

using namespace std;

int main()
{
  Cadastro cadastro = Cadastro();
  vector<Candidato> candidatosDF = cadastro.cadastro_candidatosDF(); //vetor do tipo Candidato chamado candidatosDF
  vector<Candidato> candidatosBR = cadastro.cadastro_candidatosBR(); //vetor do tipo Candidato chamado candidatosDF
  int quantidade, maior_df = 0, maior_dd = 0, maior_sen1 = 0,maior_gov = 0, maior_pres = 0;
  int escolha;
  int depf = 0, depd = 0, sen1 = 0, sen2 = 0, gov = 0, pres = 0;
  int s1a = 0, s2a = 0, s1b = 0, s2b = 0, vicegov = 0, vice = 0;
  string vencedor_df, vencedor_dd, vencedor_sen1, vencedor_sen2, vencedor_gov, vencedor_pres;
  string eleitor;
  string deputado_federal;
  string deputado_distrital;
  string senador1;
  string senador2;
  string governador;
  string presidente;
  string vencedor;
  vector<Voto> votos;

  //URNA
  cout << "Quantidade de Eleitores: ";
  cin >> quantidade;

  for (int i = 0; i < quantidade; i++)
  {

    cout << "\n\nEleições DF 2018" << endl;
    cout << "Nome do eleitor : ";
    cin.ignore();
    getline(cin, eleitor);
    //           DEPUTADO FEDERAL
    escolha = 0;
    while (escolha != 1 || escolha != 3 || escolha != 4)
    {
      cout << "\nDeputado Federal: ";
      cin >> deputado_federal;

      //Encontrando o candidato digitado no arquivo
      for (unsigned j = 0; j < candidatosDF.size(); j++)
      {
        if (deputado_federal == candidatosDF[j].get_nr_urna())
        {
          depf = j;
          break;
        }
      }
      //Caso digite um número que não seja de Deputado Federal
      if (candidatosDF[depf].get_cargo() != "DEPUTADO FEDERAL")
      {
        cout << "\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      //Caso digite um número que seja de Deputado Federal
      if (candidatosDF[depf].get_cargo() == "DEPUTADO FEDERAL")
      {
        cout << "\nDeputado Federal escolhido: " << candidatosDF[depf].get_nome_urna() << endl;
        cout << "Partido: " << candidatosDF[depf].get_nome_partido() << endl;
        cout << "\nPara CONFIRMAR digite 1\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 1 && escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      if (escolha == 1)
      {
        candidatosDF[depf].cont++;
        break;
      }
      if (escolha == 3)
      {
        depf = 0;
        break;
      }
      if (escolha == 4)
      {
        depf = 1;
        break;
      }
    }

    //           DEPUTADO DISTRITAL
    escolha = 0;
    while (escolha != 1 || escolha != 3 || escolha != 4)
    {
      cout << "\nDeputado Distrital : ";
      cin >> deputado_distrital;
      //Encontrando o candidato digitado no arquivo
      for (unsigned j = 0; j < candidatosDF.size(); j++)
      {
        if (deputado_distrital == candidatosDF[j].get_nr_urna())
        {
          depd = j;
          break;
        }
      }
      //Caso digite um número que não seja de Deputado Distrital
      if (candidatosDF[depd].get_cargo() != "DEPUTADO DISTRITAL")
      {
        cout << "\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      //Caso digite um número que seja de Deputado Distrital
      if (candidatosDF[depd].get_cargo() == "DEPUTADO DISTRITAL")
      {
        cout << "\nDeputado Distrital escolhido: " << candidatosDF[depd].get_nome_urna() << endl;
        cout << "Partido: " << candidatosDF[depd].get_nome_partido() << endl;
        cout << "\nPara CONFIRMAR digite 1\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 1 && escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      if (escolha == 1)
      {
        candidatosDF[depd].cont++;
        break;
      }
      if (escolha == 3)
      {
        depd = 0;
        break;
      }
      if (escolha == 4)
      {
        depd = 1;
        break;
      }
    }

    //           SENADOR(1ª OPÇÃO)
    escolha = 0;
    while (escolha != 1 || escolha != 3 || escolha != 4)
    {
      cout << "\nSenador(1ª Opção) : ";
      cin >> senador1;
      //Encontrando o candidato digitado no cadastro
      for (unsigned j = 0; j < candidatosDF.size(); j++)
      {
        if (senador1 == candidatosDF[j].get_nr_urna())
        {
          sen1 = j;
          break;
        }
      }
      //Caso digite um número que não seja de Senador ---------- no caso utilizamos o do 1º suplente pois é o primeiro nº que aparece no cadastro
      if (candidatosDF[sen1].get_cargo() != "1º SUPLENTE")
      { // É o primeiro que aparece no arquivo
        cout << "\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      //Caso digite um número que seja de Senador ---------- no caso utilizamos o do 1º suplente pois é o primeiro nº que aparece no cadastro
      if (candidatosDF[sen1].get_cargo() == "1º SUPLENTE")
      {

        if (candidatosDF[sen1].get_cargo() == "1º SUPLENTE")
        {
          s1a = sen1;
          sen1 = 0;
          for (unsigned j = s1a + 1; j < candidatosDF.size(); j++)
          {
            if (senador1 == candidatosDF[j].get_nr_urna())
            {
              sen1 = j;
              break;
            }
          }
        }

        if (candidatosDF[sen1].get_cargo() == "2º SUPLENTE")
        {
          s2a = sen1;
          sen1 = 0;
          for (unsigned j = s2a + 1; j < candidatosDF.size(); j++)
          {
            if (senador1 == candidatosDF[j].get_nr_urna())
            {
              sen1 = j;
              break;
            }
          }
        }

        cout << "\nSenador(1ª Opção) escolhido: " << candidatosDF[sen1].get_nome_urna() << endl;
        cout << "Partido: " << candidatosDF[sen1].get_nome_partido() << endl;
        cout << "1º Suplente: " << candidatosDF[s1a].get_nome_urna() << endl;
        cout << "2º Suplente: " << candidatosDF[s2a].get_nome_urna() << endl;
        cout << "\nPara CONFIRMAR digite 1\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 1 && escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      if (escolha == 1)
      {
        candidatosDF[sen1].cont++;
        break;
      }
      if (escolha == 3)
      {
        senador1 = "";
        sen1 = 0;
        break;
      }
      if (escolha == 4)
      {
        senador1 = "";
        sen1 = 1;
        break;
      }
    }

    //           SENADOR(2ª OPÇÃO)
    escolha = 0;
    while (escolha != 1 || escolha != 3 || escolha != 4)
    {
      cout << "\nSenador(2ª Opção) : ";
      cin >> senador2;
      //Encontrando o candidato digitado no cadastro
      for (unsigned j = 0; j < candidatosDF.size(); j++)
      {
        if (senador2 == candidatosDF[j].get_nr_urna())
        {
          sen2 = j;
          break;
        }
      }
      //Caso digite um número que não seja de Senador ---------- no caso utilizamos o do 1º suplente pois é o primeiro nº que aparece no cadastro
      if (candidatosDF[sen2].get_cargo() != "1º SUPLENTE")
      { // É o primeiro que aparece no arquivo
        cout << "\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      //Caso digite um número que seja de Senador ---------- no caso utilizamos o do 1º suplente pois é o primeiro nº que aparece no cadastro
      if (candidatosDF[sen2].get_cargo() == "1º SUPLENTE")
      {
        escolha = 0;

        if (candidatosDF[sen2].get_cargo() == "1º SUPLENTE")
        {
          s1b = sen2;
          sen2 = 0;
          for (unsigned j = s1b + 1; j < candidatosDF.size(); j++)
          {
            if (senador2 == candidatosDF[j].get_nr_urna())
            {
              sen2 = j;
              break;
            }
          }
        }

        if (candidatosDF[sen2].get_cargo() == "2º SUPLENTE")
        {
          s2b = sen2;
          sen2 = 0;
          for (unsigned j = s2b + 1; j < candidatosDF.size(); j++)
          {
            if (senador2 == candidatosDF[j].get_nr_urna())
            {
              sen2 = j;
              break;
            }
          }
        }
        //Caso digite um número que foi digitado em Senador 1ª Opção
        if (senador2 == senador1)
        {
          cout << "Candidato já escolhido\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
          cin >> escolha;
          while (escolha != 2 && escolha != 3 && escolha != 4)
          {
            cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
            cin >> escolha;
          }
        }
        else
        {
          cout << "\nSenador(2ª Opção) escolhido: " << candidatosDF[sen2].get_nome_urna() << endl;
          cout << "Partido: " << candidatosDF[sen2].get_nome_partido() << endl;
          cout << "1º Suplente: " << candidatosDF[s1b].get_nome_urna() << endl;
          cout << "2º Suplente: " << candidatosDF[s2b].get_nome_urna() << endl;
          cout << "\nPara CONFIRMAR digite 1\nPara CORRIGIR digite 2\nPara votar NULO digite 3\n";
          cin >> escolha;
          while (escolha != 1 && escolha != 2 && escolha != 3 && escolha != 4)
          {
            cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
            cin >> escolha;
          }
        }
      }
      if (escolha == 1)
      {
        candidatosDF[sen2].cont++;
        break;
      }
      if (escolha == 3)
      {
        sen2 = 0;
        break;
      }
      if (escolha == 4)
      {
        sen2 = 1;
        break;
      }
    }

    //           GOVERNADOR E VICE-GOVERNADOR
    escolha = 0;
    while (escolha != 1 || escolha != 3 || escolha != 4)
    {
      cout << "\nGovernador : ";
      cin >> governador;
      //Encontrando o candidato digitado no cadastro
      for (unsigned j = 0; j < candidatosDF.size(); j++)
      {
        if (governador == candidatosDF[j].get_nr_urna())
        {
          gov = j;
          break;
        }
      }
      //Caso digite um número que não seja de Governador  ----------------------- no caso utilizamos o de vice-governador pois é o primeiro que aparece no cadastro
      if (candidatosDF[gov].get_cargo() != "VICE-GOVERNADOR")
      { // É o primeiro que aparece no arquivo
        cout << "\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      //Caso digite um número que seja de Governador ---------- no caso utilizamos o do vice-governador pois é o primeiro nº que aparece no cadastro
      if (candidatosDF[gov].get_cargo() == "VICE-GOVERNADOR")
      {

        if (candidatosDF[gov].get_cargo() == "VICE-GOVERNADOR")
        {
          vicegov = gov;
          gov = 0;
          for (unsigned j = vicegov + 1; j < candidatosDF.size(); j++)
          {
            if (governador == candidatosDF[j].get_nr_urna())
            {
              gov = j;
              break;
            }
          }
        }

        cout << "\nGovernador escolhido: " << candidatosDF[gov].get_nome_urna() << endl;
        cout << "Partido: " << candidatosDF[gov].get_nome_partido() << endl;
        cout << "Vice Governador: " << candidatosDF[vicegov].get_nome_urna() << endl;
        cout << "\nPara CONFIRMAR digite 1\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 1 && escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      if (escolha == 1)
      {
        candidatosDF[gov].cont++;
        break;
      }
      if (escolha == 3)
      {
        gov = 0;
        break;
      }
      if (escolha == 4)
      {
        gov = 0;
        break;
      }
    }

    //           PRESIDENTE E VICE-PRESIDENTE
    escolha = 0;
    while (escolha != 1 || escolha != 3 || escolha != 4)
    {
      cout << "\nPresidente : ";
      cin >> presidente;
      //Encontrando o candidato digitado no cadastro
      for (unsigned j = 0; j < candidatosBR.size(); j++)
      {
        if (presidente == candidatosBR[j].get_nr_urna())
        {
          pres = j;
          break;
        }
      }
      //Caso digite um número que não seja de Presidente  ----------------------- no caso utilizamos o de vice-presidente pois é o primeiro que aparece no cadastro
      if (candidatosBR[pres].get_cargo() != "VICE-PRESIDENTE")
      { // É o primeiro que aparece no arquivo
        cout << "\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      //Caso digite um número que seja de Governador ---------- no caso utilizamos o do vice-governador pois é o primeiro nº que aparece no cadastro
      if (candidatosBR[pres].get_cargo() == "VICE-PRESIDENTE")
      {

        if (candidatosBR[pres].get_cargo() == "VICE-PRESIDENTE")
        {
          vice = pres;
          pres = 0;
          for (unsigned j = vice + 1; j < candidatosBR.size(); j++)
          {
            if (presidente == candidatosBR[j].get_nr_urna())
            {
              pres = j;
              break;
            }
          }
        }

        cout << "\nPresidente escolhido: " << candidatosBR[pres].get_nome_urna() << endl;
        cout << "Partido: " << candidatosBR[pres].get_nome_partido() << endl;
        cout << "Vice Presidente: " << candidatosBR[vice].get_nome_urna() << endl;
        cout << "\nPara CONFIRMAR digite 1\nPara CORRIGIR digite 2\nPara votar NULO digite 3\nPara votar BRANCO digite 4\n";
        cin >> escolha;
        while (escolha != 1 && escolha != 2 && escolha != 3 && escolha != 4)
        {
          cout << "NÚMERO INVÁLIDO!\nDigite Novamente" << endl;
          cin >> escolha;
        }
      }
      if (escolha == 1)
      {
        candidatosBR[pres].cont++;
        break;
      }
      if (escolha == 3)
      {
        pres = 0;
        break;
      }
      if (escolha == 4)
      {
        pres = 0;
        break;
      }
    }
  
    cout << "\n\n------------FIM------------\n\n";
    Voto votoOK = Voto(eleitor, candidatosDF[depf].get_nome_urna(), candidatosDF[depd].get_nome_urna(), candidatosDF[sen1].get_nome_urna(), candidatosDF[sen2].get_nome_urna(), candidatosDF[gov].get_nome_urna(), candidatosBR[pres].get_nome_urna());
    votos.push_back(votoOK);

    depf = 0;
    depd = 0;
    sen1 = 0;
    s1a = 0;
    s2a = 0;
    sen2 = 0;
    s1b = 0;
    s2b = 0;
    gov = 0;
    vicegov = 0;
    pres = 0;
    vice = 0;
  }

  for (int i = 0; i < quantidade; i++)
  {
    cout << "\n-------Relatório de Votos-------" << endl;
    cout << "\nNome do Eleitor: " << votos[i].get_nome_eleitor() << endl;
    cout << "\nDeputado Federal escolhido: " << votos[i].get_deputado_federal() << endl;
    cout << "Deputado Distrital escolhido: " << votos[i].get_deputado_distrital() << endl;
    cout << "Senador(1ª Opção) escolhido: " << votos[i].get_senador1() << endl;

    cout << "Senador(2ª Opção) escolhido: " << votos[i].get_senador2() << endl;

    cout << "Governador escolhido: " << votos[i].get_governador() << endl;

    cout << "Presidente: " << votos[i].get_presidente() << endl;

    cout << "\n----------FIM-------------\n\n"
         << endl;
  }

  // QUEM GANHOU ?

  //Deputado Federal
  for (unsigned i = 0; i < candidatosDF.size(); i++)
  {
    if (candidatosDF[i].get_cargo() == "DEPUTADO FEDERAL")
    {

      if (candidatosDF[i].cont > maior_df)
      {
        maior_df = candidatosDF[i].cont;
        vencedor_df = candidatosDF[i].get_nome_urna();
      }
    }
  }
  if (maior_df == 0)
  {
    vencedor_df = "Não houve vencedor";
  }
  //Deputado Distrital
  for (unsigned i = 0; i < candidatosDF.size(); i++)
  {
    if (candidatosDF[i].get_cargo() == "DEPUTADO DISTRITAL")
    {

      if (candidatosDF[i].cont > maior_dd)
      {
        maior_dd = candidatosDF[i].cont;
        vencedor_dd = candidatosDF[i].get_nome_urna();
      }
    }
  }
  if (maior_dd == 0)
  {
    vencedor_dd = "Não houve vencedor";
  }
  //Senador 1
  for (unsigned i = 0; i < candidatosDF.size(); i++)
  {
    if (candidatosDF[i].get_cargo() == "SENADOR")
    {

      if (candidatosDF[i].cont > maior_sen1)
      {
        maior_sen1 = candidatosDF[i].cont;
        vencedor_sen1 = candidatosDF[i].get_nome_urna();
      }
    }
  }
  if (maior_sen1 == 0)
  {
    vencedor_sen1 = "Não houve vencedor";
  }
  //Governador
  for (unsigned i = 0; i < candidatosDF.size(); i++)
  {
    if (candidatosDF[i].get_cargo() == "GOVERNADOR")
    {

      if (candidatosDF[i].cont > maior_gov)
      {
        maior_gov = candidatosDF[i].cont;
        vencedor_gov = candidatosDF[i].get_nome_urna();
      }
    }
  }
  if (maior_gov == 0)
  {
    vencedor_gov = "Não houve vencedor";
  }
  //Presidente
  for (unsigned i = 0; i < candidatosBR.size(); i++)
  {
    if (candidatosBR[i].get_cargo() == "PRESIDENTE")
    {

      if (candidatosBR[i].cont > maior_pres)
      {
        maior_pres = candidatosBR[i].cont;
        vencedor_pres = candidatosBR[i].get_nome_urna();
      }
    }
  }
  if (maior_pres == 0)
  {
    vencedor_pres = "Não houve vencedor";
  }
  cout << "\n\n--------VENCEDORES ----------\n\n";
  cout << "DEPUTADO FEDERAL = " << vencedor_df << endl;
  cout << "DEPUTADO DISTRITAL = " << vencedor_dd << endl;
  cout << "SENADOR = " << vencedor_sen1 << endl;
  cout << "GOVERNADOR= " << vencedor_gov << endl;
  cout << "PRESIDENTE= " << vencedor_pres << endl;

  return 0;
}