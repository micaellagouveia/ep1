# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto
- make clean
- make
- make run

## Funcionalidades do projeto
A urna tem as seguintes funções:
- Inserir numero do respectivo candidato
- Corrige o número
- Voto em branco e nulo
- Confirma
- Relatorio da relação do eleitor com seus votos
- Vencedor de cada cargo

## Bugs e problemas

## Referências
- Possui a classe mãe Pessoa, que possui o nome, idade e genero.
- A classe Candidato herda da classe pessoa e ganha os atriubutos nome na urna, numero na urna, nome do partido, estado, cargo.
- Há também a classe voto, na qual armazeno os votos, de cada eleitor, de todos os cargos.
- Há também a classe cadastro, nela abri o aquivo csv,separei os dados, e os armazenei em um vetor do tipo Candidato. Esse vetor foi utilizado na main para acessar os dados do arquivo, quando o eleitor escolhia seu candidato.
- No cadastro, governador e vice-governador possuem o mesmo número. Isso também vale para presidente e senador, que possui seus suplentes. Na main, fiz a distinção de governador e vice, e para os demais também.
- Ao final de cada votação, instancio um objeto do tipo voto, e armazeno seus dados em um vetor do tipo voto. Esse vetor, uso para o relatório da relação eleitor e seus votos.
- Faço comparações no arquivo , com cada cargo, para saber quem ganhou mais votos, e anuncio o vencedor no final do programa
- No programa, comentei algumas partes para melhor entendimento.